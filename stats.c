/******************************************************************************
 * Copyright (C) 2017 by Alex Fosdick - University of Colorado
 *
 * Redistribution, modification or use of this software in source or binary
 * forms is permitted as long as the files maintain this copyright. Users are 
 * permitted to modify this and use it to learn about the field of embedded
 * software. Alex Fosdick and the University of Colorado are not liable for any
 * misuse of this material. 
 *
 *****************************************************************************/
/**
 * @file stats.c
 * @brief main() - The main entry point for your program
	print_statistics() - A function that prints the statistics of an array including minimum, 		maximum, mean, and median.
	print_array() - Given an array of data and a length, prints the array to the screen
	find_median() - Given an array of data and a length, returns the median value
	find_mean() - Given an array of data and a length, returns the mean
	ind_maximum() - Given an array of data and a length, returns the maximum
	find_minimum() - Given an array of data and a length, returns the minimum
	sort_array() - Given an array of data and a length, sorts the array from largest to smallest. 		(The zeroth Element should be the largest value, and the last element (n-1)should be the 	smallest value)
 *
 *
 * @author Nithin Mohan
 * @date 5th April 2022
 *
 */



#include <stdio.h>
#include "stats.h"

/* Size of the Data Set */
#define SIZE (40)

/*void print_statistics(array) ;*/

void main()
{

  unsigned char test[SIZE] = { 34, 201, 190, 154,   8, 194,   2,   6,
                              114, 88,   45,  76, 123,  87,  25,  23,
                              200, 122, 150, 90,   92,  87, 177, 244,
                              201,   6,  12,  60,   8,   2,   5,  67,
                                7,  87, 250, 230,  99,   3, 100,  90};



  /* Other Variable Declarations Go Here */
  /* Statistics and Printing Functions Go Here */

  //printf("%s",test);

  int n = (int)sizeof(test) / sizeof(test[0]);
  printf("Value of n %d \n ", n);

  print_statistics(test,n);


}

/* Add other Implementation File Code Here */


void print_statistics(unsigned char array[], int length) 
{


int max_value=find_maximum(array, length);
int min_value=find_minimum(array, length);
float mean_value=find_mean(array, length);

print_array(array, length);
sort_array(array,length);
print_array(array, length);
float median=find_meadian(array, length);
}



int find_maximum(unsigned char array[], int length)
{
	int max=0;
	for(int i=0; i<length; i++){	
		if(i==0) max=(int)array[0];
		
		else if(max<=(int)array[i]){
			max=(int)array[i];
			}

		else max=max;
		}
	printf("The maximum value is: %d \n", max);

return max;
}

int find_minimum(unsigned char array[], int length)
{
	int min=0;
	for(int i=0; i<length; i++){	
		if(i==0) min=(int)array[0];
		
		else if(min>=(int)array[i]){
			min=(int)array[i];
			}

		else min=min;
		}
	printf("The minimum value is: %d \n", min);
return min;
}

void print_array(unsigned char array[], int length)
{
printf("Values of array [");
for(int i=0; i<length; i++){
	if(i<(length-1)){
		printf("%d , ",array[i]);
		}
	else{
		printf("%d",array[i]);
		}
	}
printf("]\n");
}

float find_mean(unsigned char array[], int length)
{
float mean=0;
int sum=0;
for (int i=0; i<length;i++){
	sum+=array[i];
	}
mean=sum/length;
printf("Mean value is : %.2f \n", mean);
return mean;
}


float find_meadian(unsigned char array[], int length)
{
float meadian=0;
if(length%2!=0){
	meadian=array[length/2];
	}
else{
	meadian= (array[(length-1)/2]+array[length/2])/2;
	}
printf("Meadian value is : %.2f \n", meadian);
return meadian;
}

void swap(unsigned char* xp, unsigned char* yp)
{
    int temp = *xp;
    *xp = *yp;
    *yp = temp;
}

void sort_array(unsigned char array[], int length)
{
    int i, j, min_idx;
 
    // One by one move boundary of unsorted subarray
    for (i = 0; i < length - 1; i++) {
 
        // Find the minimum element in unsorted array
        min_idx = i;
        for (j = i + 1; j < length; j++)
            if (array[j] < array[min_idx])
                min_idx = j;
 
        // Swap the found minimum element
        // with the first element
        swap(&array[min_idx], &array[i]);
    }
}

include source.mk
#Override
CPU=coretex-m0plus
ARCH=thumb
SPECS=nosys.specs

#Compiler
CC=arm-none-eabi-gcc
LD=arm-node-enabi-ld
CFLAG= $(CC) -mcpu=$(CPU) -m$(ARCH) --specs=$(SPECS)
BASENAME=demo
TARGET=$(BASENAME).out
LDFLAG= -WI, -map=$(BASENAME).map

OBJC=$(SRCS:.c=.o)

%.o:%.c
	$(CC) $(CFLAG) -c $(^) -o $(@)

.PHONY: build
build:all

.PHONY: all
all:  $(TARGET)

$(TARGET):$(OBJC)
	$(CC) $(LD) $(CFLAG) $(LDFLAG) -o $(TARGET) $(OBJC)

$PHONY: clean
clean:
	rm -f $(OBJC) $(TARGET) $(BASENAME).map
 








